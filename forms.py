from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, PasswordField, BooleanField
from wtforms.validators import DataRequired, Email
import email_validator


class Project_form(FlaskForm):
	title = StringField('Название проекта', validators=[DataRequired()], render_kw={"placeholder": "Название проекта",
																					"class": "form__reg-input"})
	FIO = StringField('Ваше ФИО', validators=[DataRequired()], render_kw={"placeholder": "Ваше ФИО",
																		  "class": "form__reg-input"})
	phone = StringField('Ваш телефон', validators=[DataRequired()], render_kw={"placeholder": "Ваш телефон",
																			   "class": "form__reg-input"})
	email = StringField('Ваш email', validators=[Email("Почта введена неверно")], render_kw={"placeholder": "Ваш email",
																							 "class": "form__reg-input"})
	password = PasswordField('Придумайте пароль', validators=[DataRequired()],
							 render_kw={"placeholder": "Придумайте пароль",
										"class": "form__reg-input"})
	submit = SubmitField('Зарегистрироваться', render_kw={"placeholder": "Зарегистрироваться",
														  "class": "btn_submit reg_btn"})

class Login_form(FlaskForm):
	title = StringField('Название проекта', validators=[DataRequired()], render_kw={"placeholder": "Название проекта",
																					"class": "data"})
	email = StringField('Ваш email', validators=[Email("Почта введена неверно")], render_kw={"placeholder": "Ваш email",
																							 "class": "data"})
	password = PasswordField('Пароль', validators=[DataRequired()],
							 render_kw={"placeholder": "Введите пароль",
										"class": "data"})
	remember = BooleanField('Запомнить меня')
	submit = SubmitField('Войти', render_kw={"class": "btn_submit btn_submit-modal"})
