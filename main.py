import os
from config import app, db
from datetime import datetime, date
from flask import Flask, request, render_template, redirect, url_for, json, session
from flask_mail import Mail, Message
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager, UserMixin, login_required, login_user, current_user, logout_user
from werkzeug.security import generate_password_hash
from werkzeug.utils import secure_filename

mail = Mail(app)
login_manager = LoginManager(app)
login_manager.login_view = 'index'

from create_db import Project, Stage
from forms import Project_form, Login_form


@login_manager.user_loader
def load_user(id):
	return db.session.query(Project).get(id)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('index'))



PRICE_LIST = {'landing': 20000, 'multipage': 30000,
			  'store': 40000, 'catalog': 30000, 'forum': 35000, 'agregator': 38000, 'corporat': 40000,
			  'undeterm': 30000,
			  'yes': 10000, 'no': 0,
			  'templ': 8000, 'individual': 15000, 'clients': 0,
			  'translate': 8000, 'robot': 15000, 'mailer': 3000}

FORM_NAMES = ['site_type', 'SEO', 'design']


@app.route("/", methods=["GET", "POST"])
def index():
	session['reg'] = False
	modal = "none"
	error = "none"
	if current_user.is_authenticated:
		developer = False
		project = date_format(current_user)
		return render_template("devstages.html", project=project, developer=developer)
	else:
		form = Login_form()
	if request.method == 'POST':
		try:
			db_user = request.form['_name']
			db_email = request.form['_email']
			msg = Message("Вы оставили форму обратной связи", sender="", recipients=[db_email])
			msg.body = "Здравствуйте! Вы оставили форму обратной связи. Скоро вам перезвонят наши операторы! {} <{}>.".format(
				db_user, db_email)
			mail.send(msg)
		except KeyError:
			db_user = None
			db_tel = None
			db_email = None
		if form.validate_on_submit():
			data = form.data
			session['data'] = data
			return redirect(url_for('log_in', firstLogin=False))
		else:
			modal = "block"
			error = "none"
	if request.args.get('error') is not None:
		modal = "block"
		error = "block"
	return render_template("index.html", form=form, ModalDisplay=modal, ErrorDisplay=error)


@app.route("/form/<int:i>", methods=["GET", "POST"])
def calc(i):
	if 'price' not in session or i == 1:
		session['price'] = 0
		session['already'] = [False, False, False, False]
	if request.method == "POST":
		try:
			subs = request.form['subsite_type']
		except KeyError:
			subs = None
		if not subs:
			if session['already'][i-1]:
				session['price'] -= PRICE_LIST[request.form[FORM_NAMES[i - 1]]]
			session['price'] += PRICE_LIST[request.form[FORM_NAMES[i - 1]]]
		else:
			session['price'] += PRICE_LIST[request.form['subsite_type']]
		session['already'][i-1] = True
		return redirect('/form/' + str(i + 1))
	return render_template("form" + str(i) + ".html", num=i)


@app.route("/form_last/<int:i>", methods=["GET", "POST"])
def get_price(i):
	output = ''
	if request.method == "POST":
		checked = request.form.getlist('addition')
		for point in checked:
			if session['already'][i-1]:
				session['price'] -= PRICE_LIST[point]
			session['price'] += PRICE_LIST[point]
		output = 'Стоимость разработки от {} рублей'.format(session['price'])
		session['already'][i - 1] = True
		try:
			registration = session['reg']
		except KeyError:
			registration = False
	return render_template("price.html", num=i, price=output, registration=registration)


@app.route("/sign_in", methods=["GET", "POST"])
def sign_in():
	form = Project_form()
	display = "none"
	emailDisplay = "none"
	if request.method == "POST":
		if form.validate_on_submit():
			try:
				msg = Message("Регистрация проекта", sender="", recipients=[form.email.data])
				msg.boby = "Проект {} успешно зарегистрирован!".format(form.title.data)
				mail.send(msg)
			except:
				form = Project_form()
				emailDisplay = "block"
				return render_template("registration.html", form=form, emailDisplay=emailDisplay)
			data = form.data
			title = form.title.data
			data.pop('submit')
			data.pop('csrf_token')
			data['password_hash'] = generate_password_hash(data['password'])
			data.pop('password')
			record = Project(**data)
			db.session.add(record)
			db.session.commit()
			result = Project.query.filter_by(title=title).first()
			result.date = result.date.strftime("%d %b %Y ")
			data['date'] = result.date
			session['project'] = data
			session['reg'] = True
			
			return redirect('form/1')
		else:
			display = "block"
	return render_template("registration.html", form=form, display=display, emailDisplay=emailDisplay)


@app.route("/continue", methods=["GET", "POST"])
def _continue():
	return redirect(url_for('log_in', firstLogin=True))


@app.route("/personal_room", methods=["GET", "POST"])
def log_in():
	firstLogin = request.args.get('firstLogin')
	if firstLogin == "False":
		developer = False
		project = False
		columns = session['data']
		check = Project.query.filter(Project.title == columns['title'],
									 Project.email == columns['email']).first()
		if check and check.check_password(columns['password']):
			project = check
			if columns['remember']:
				login_user(project, remember=columns['remember'])
		elif columns['email'] == app.config['MAIL_USERNAME'] and columns['password'] == app.config['MAIL_PASSWORD']:
			project = Project.query.filter_by(title=columns['title']).first()
			developer = True
		if project:
			project = date_format(project)
		else:
			return redirect(url_for('index', error=True))
	else:
		developer = False
		project = session['project']
	return render_template("devstages.html", project=project, developer=developer)


@app.route("/add_stage/<int:id>", methods=["GET", "POST"])
def adding(id):
	if request.method == "POST":
		developer = True
		file = request.files['screen']
		filename = secure_filename(file.filename)
		file.save(os.path.join('.\static\stage_images', filename))
		stage = Stage(title=request.form['title'], text=request.form['description'], image=filename, project_id=id)
		db.session.add(stage)
		db.session.commit()
		project = db.session.query(Project).get(id)
		project = date_format(project)
	return render_template("devstages.html", project=project, developer=developer)


def date_format(project):
	project.date = date(project.date.year, project.date.month, project.date.day)
	for stage in project.stages:
		stage.date = date(stage.date.year, stage.date.month, stage.date.day)
	return project
