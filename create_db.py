import sqlite3
from config import db
from datetime import datetime
from flask_login import LoginManager, UserMixin
from werkzeug.security import generate_password_hash,  check_password_hash


class Project(db.Model, UserMixin):
    __tablename__ = 'projects'
    id = db.Column(db.Integer(), primary_key=True, autoincrement='auto')
    title = db.Column(db.String(64), unique=True)
    FIO = db.Column(db.String(64))
    phone = db.Column(db.String(20))
    email = db.Column(db.String(120))
    password_hash = db.Column(db.String(64))
    date = db.Column(db.DateTime(), default=datetime.now())
    stages = db.relationship('Stage')

    def __repr__(self):
        return 'Проект: ' + self.title

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class Stage(db.Model):
    __tablename__ = 'stages'
    id = db.Column(db.Integer(), primary_key=True, autoincrement='auto')
    title = db.Column(db.String(64))
    text = db.Column(db.Text(500))
    image = db.Column(db.String(64), nullable=True)
    date = db.Column(db.DateTime(), default=datetime.now())
    project_id = db.Column(db.Integer(), db.ForeignKey('projects.id'))

    def __repr__(self):
        return 'Этап: ' + self.title
