import os
from flask import Flask
from flask_mail import Mail, Message
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager, UserMixin, login_required


app = Flask(__name__)

basedir = os.path.abspath(os.path.dirname(__file__))

app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'database.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['MAIL_SERVER'] = 'smtp.mail.ru'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'webx.info@mail.ru'  # введите свой адрес электронной почты здесь
app.config['MAIL_DEFAULT_SENDER'] = 'webx.info@mail.ru'  # и здесь
app.config['MAIL_PASSWORD'] = 'one1two2'
db = SQLAlchemy(app)
migrate = Migrate(app, db, render_as_batch=True)

from main import *

if __name__ == '__main__':
	app.run(debug=True)

