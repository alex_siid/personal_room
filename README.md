# README #
<h1>ИНСТРУКЦИЯ</h1>
Для входа в проект под разработчиком(чтоб была возможность добавлять этапы к проекту) нужно ввести почту webx.info@mail.ru и пароль one1two2.
<h3>Проект "Личный кабинет пользователя" - финал на пути к разработке готового сайта.</h3>

<ul>
На третьем шаге перед нами была задача подключить к сайту базу данных для сбора и хранения информации о клиентах.
Разработка третьего этапа включала в себя:
        <ul>
            <li>Создание базы данных и файла со скриптами миграций</li>
            <li>Создание личного кабинета для пользователей и разработчиков</li>
            <li>Реализация обработки формы регистрации, добавление ещё одной базы данных для этапов заказанных проектов</li>
            <li>Добавление функции входа в личный кабинет и этапов, обеспечение вывода данных о проекте с помощью jinja</li>
            <li>Создание и стилизацию форма авторизации проектов, добавление сообщения о ошибке ввода</li>
            <li>Добавление возможности расчёта стоимости сайта после регистрации</li>
            <li>Обеспечение работы кнопки «Купить» на главной странице, а также отправки писем на почту при успешной регистрации</li>
            <li>Исправление ошибок и недочетов</li>
        </ul>
Наша команда:
        <ul>
            <li>1. Катаев Даниил </li>
            <li>2. Сидоров Александр </li>
            <li>3. Таразанова Дарина </li>
            <li>4. Семерикова Екатерина </li>
        </ul>

</ul>